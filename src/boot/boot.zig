comptime {
    asm (@embedFile("boot64.s"));
    asm (@embedFile("boot32.s"));
}

const MAGIC = 0xE85250D6;
const ARCHITECTURE = 0;

const EndTag = packed struct {
    type: u16 = 0,
    flags: u16 = 0,
    size: u32 = 8,
};

const MultibootHeader = extern struct {
    magic: u32,
    architecture: u32,
    header_len: u32,
    checksum: u32,
    endtag: EndTag,
};

export const multiboot2 align(4) linksection(".multiboot") = MultibootHeader{
    .magic = MAGIC,
    .architecture = ARCHITECTURE,
    .header_len = 24,
    .checksum = 0x100000000 - (MAGIC + ARCHITECTURE + 24),
    .endtag = EndTag{},
};
