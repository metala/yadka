.global boot_long_mode_start
.extern kmain

.section .text
.code64
boot_long_mode_start:
    // load null into all data segment registers
    mov $0, %ax
    mov %ax, %ss 
    mov %ax, %ds 
    mov %ax, %es 
    mov %ax, %fs 
    mov %ax, %gs 

	jmp kmain