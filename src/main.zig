comptime {
    _ = @import("boot/boot.zig");
}

const debug = @import("debugcon.zig");
const console = @import("console.zig");

export fn kmain() noreturn {
    console.initialize();
    console.setColor(0x2F);
    console.puts("64-bit Yadka");

    // write debug data
    debug.printf("Hi from {p}\n", .{&kmain});

    while (true) {}
}
